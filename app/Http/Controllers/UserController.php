<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function getAll()
    {
        return response()->json(Users::all());
    }

    public function showAllusers()
    {
      	return response()->json(Users::all());
    }

    public function showOneUser($id)
    {
        return response()->json(Users::find($id));
    }

    public function createUser(Request $request)
    {
        $user = Users::create($request->all());

        return response()->json($user, 201);
    }

    public function update($id, Request $request)
    {
        $user = Users::findOrFail($id);
        $user->update($request->all());

        return response()->json($user, 200);
    }

    public function delete($id)
    {
        Users::findOrFail($id)->delete();
        return response()->json(null, 204);
    }
}