<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'Sinisa',
            'lastname' => 'Botic',
            'email' => 'boticsinisa@gmail.com',
            'password' => Hash::make('secret'),
            'role'     => 'superadmin',
        ]);
        DB::table('users')->insert([
            'firstname' => 'Filip',
            'lastname' => 'Botic',
            'email' => 'filip@gmail.com',
            'password' => Hash::make('secret'),
            'role'     => 'editor',
        ]);
        DB::table('users')->insert([
            'firstname' => 'Fran',
            'lastname' => 'Botic',
            'email' => 'fran@gmail.com',
            'password' => Hash::make('secret'),
            'role'     => 'editor',
        ]);
        DB::table('users')->insert([
            'firstname' => 'Anica',
            'lastname' => 'Botic',
            'email' => 'anica@gmail.com',
            'password' => Hash::make('secret'),
            'role'     => 'editor',
        ]);
    }
}
