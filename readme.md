# Holidayextras-User Management 
Lumen Vue REST API



#### Short description
This is simple SPA built by <a href="https://lumen.laravel.com/">Lumen</a> with SQLite database, powered with <a href="https://vuejs.org/">VueJS</a> on frontend part. PHP Test are made with PHPUnit and API documentation is generated and exported with Postman.


## Features

- Lumen 5.5
- Testing in Laravel style
- Serve command
- Support Form Request Validation
- Vue 
- Bootstrap 4 + Font Awesome 5

## Installation

- `download repo: git clone git@gitlab.com:holidayextras/user-management.git`
- `composer install`
- `php artisan migrate --seed`
- `npm install`



### Development

```bash
# build and watch
npm run watch

# localhost development
composer start

# localhost testing with phpunit
composer test

```

### Production

```bash
npm run build
```


### Contributors
Sinisa Botic
