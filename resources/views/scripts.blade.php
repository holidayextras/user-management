{{-- Global configuration object --}}
@php
$config = [
    'appName' => config('app.name'),
    'locale' => $locale = config('app.locale'),
    'locales' => config('app.locales'),
];
@endphp
<script>window.config = @json($config);</script>

{{-- Load the application scripts --}}
@if (env('APP_ENV') === 'local')
  <script src="dist/app.js"></script>
@else
  <script src="js/manifest.js"></script>
  <script src="js/vendor.js"></script>
  <script src="js/app.js"></script>
@endif
