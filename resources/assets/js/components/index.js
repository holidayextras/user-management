import Vue from 'vue'
import Card from './Card'
import Form from './Form'

// Components that are registered globaly.
[
  Card,
  Form
].forEach(Component => {
  Vue.component(Component.name, Component)
})