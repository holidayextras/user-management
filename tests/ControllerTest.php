<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Users;

class ControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    /**
     * /user [POST]
     */
    public function testAddUser()
    {
        $params = [
            'firstname' => 'Demo',
            'lastname' => 'Test',
            'email' => 'test@gmail.com',
            'password' => 'secret2',
            'role' => 'editor'
        ];
        $this->json('POST', '/api/user/create', $params)   
             ->seeJson(['firstname' => 'Demo']);
    }


    /**
     * /user/id [GET]
     */
    public function testGetUser(){
        $this->json('GET', '/api/user/1', []);
        $this->seeStatusCode(200);
        
    }




    /**
     * /check user by name [GET]
     */
    public function testUserByName(){
        $this->seeInDatabase('users', ['firstname' => 'Sinisa']);
    }




    public function testApi()
    {
        $response = $this->call('GET', '/api/users');

        $this->assertEquals(200, $response->status());
    }
}
