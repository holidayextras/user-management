<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$router->group(['prefix' => 'api'], function ($router) {
		$router->get('/', 'UserController@getAll');
        $router->get('/users', 'UserController@showAllusers');
        $router->get('/user/{id}', 'UserController@showOneUser');
		$router->post('/user/create', 'UserController@createUser');
		$router->delete('/user/{id}', 'UserController@delete');
		$router->put('/user/{id}', 'UserController@update');

});

$router->get('/', function () {
    return view('index');
});
